package test;

import java.util.List;

import model.Level;
import model.persistanceDB.DbAccess;
import model.persistanceDB.LevelDB;

public class DatabaseTest {

	private List<Level> arrLevel;

	public List<Level> getArrLevel() {
		return arrLevel;
	}

	public void setArrLevel(List<Level> arrLevel) {
		this.arrLevel = arrLevel;
	}

	public static void main(String[] args) {
		DatabaseTest dbt = new DatabaseTest();
		LevelDB dbLevels = new LevelDB(new DbAccess()); // List<ILevel> mit Level Objekten
		dbt.setArrLevel(dbLevels.readAllLevel());

		for (Level bl : dbt.arrLevel) {
			System.out.println(bl);
		}

	}

}
