package view.painter;

import java.awt.Graphics;

public interface IPainter {

	void paint(Graphics g, long time);
}
