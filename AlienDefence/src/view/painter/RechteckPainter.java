package view.painter;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import model.Target;

public class RechteckPainter implements IPainter {

	private Target target;
	private BufferedImage image;

	public RechteckPainter(Target target) {
		this.target = target;
		try {
			this.image = ImageIO.read(new File("./pictures/" + target.getImageAddress()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void paint(Graphics g, long time) {

		long targetStartTime = target.getTime();
		long targetEndTime = target.getTime() + target.getDuration();

		// Pr�ft, ob das Target erscheint.
		if ((time < targetStartTime || time > targetEndTime))
			return;

		// System.out.println(target.isHit());
		// target.isHit()

		if (this.target.isHit()) {
			g.setColor(Color.RED);
			return;
			// g.clearRect(100, 100, 100, 100);

		} else {
			g.setColor(Color.BLUE);
		}
		g.fillRect(this.target.getHitbox().getX(), this.target.getHitbox().getY(), this.target.getHitbox().getBreite(),
				this.target.getHitbox().getHoehe());
		g.drawImage(this.image, this.target.getHitbox().getX(), this.target.getHitbox().getY(),
				this.target.getHitbox().getBreite(), this.target.getHitbox().getHoehe(), null);

	}

}
