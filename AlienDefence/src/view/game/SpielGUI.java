package view.game;

import javax.swing.JFrame;
import controller.GameController;

/**
 * GUI der Klasse _______
 * 
 * @author Tenbusch
 *
 */
public class SpielGUI extends JFrame {

	private static final long serialVersionUID = 1L;
	private Spielfeld spielfeld;
	private GameController gc;
	public final long STARTTIME;
	private final int WIDTH = 1280;
	private final int HEIGHT = 1024;

	// Konstruktor
	public SpielGUI(GameController gc) {
		super("Spiel v0.0 - FPS: ");

		// Fenstergröße und Position setzen
		setSize(WIDTH, HEIGHT);
		setLocation(1, 1);
		// feste Fenstergröße
		setResizable(false);
		// Verhalten beim Schließen vereinbaren
		// setDefaultCloseOperation(EXIT_ON_CLOSE);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		// Controller registrieren
		this.gc = gc;

		gc.setHasWon(false);

		// Spielfeld erstellen
		spielfeld = new Spielfeld(gc);
		getContentPane().add(spielfeld);

		// sichtbar machen
		setVisible(true);

		// Startzeit setzen
		STARTTIME = System.currentTimeMillis();
	}

	public void start() {

		long lastStep = System.currentTimeMillis() - 1;

		// Spielfeld vorbereiten
		// spielfeld.init(); // Doppelt initialisiert ?

		// Levelzeit starten

		gc.startLevel();

		while (true) {

			// FPS berechnen
			long delta = System.currentTimeMillis() - lastStep;
			lastStep = System.currentTimeMillis();
			setTitle("Spiel v0.0 - FPS: " + (1000 / delta));

			// Spiellogik
			this.gc.doLogik(delta);

			// neu zeichnen
			repaint();

			try {
				Thread.sleep(1);
			} catch (Exception e) {

			}
		}

	}

}
