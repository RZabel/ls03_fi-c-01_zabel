package view.menue;

import java.awt.Dimension;
import java.util.Vector;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import model.persistanceDB.TestsDB;

public class Highscore extends JFrame {

	// Attribute
	private static final long serialVersionUID = 4977015492869252518L;
	Vector<Vector<String>> vecRow;

	// Konstruktor
	public Highscore(int level_id) {
		// Zweidimensioaler Vector, mit Inhalt der Tabelle wird geholt.
		TestsDB dbTests = new TestsDB(level_id);
		vecRow = dbTests.getVecTests();

		// Spaltenüberschriften
		Vector<String> title = new Vector<>();
		title.add("Rang");
		title.add("Spieler");
		title.add("Trefferwert");
		title.add("Genauigkeitswert");
		title.add("Reaktionswert");
		title.add("Highscore-Wert");

		// Tablle basierend auf zweidimensionalem Vector
		JTable table = new JTable(vecRow, title);
		setMinimumSize(new Dimension(650, 500));
		getContentPane().add(new JScrollPane(table));
		setTitle("Highscore-Liste"); // Titel
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		pack();
		setVisible(true);
	}
}