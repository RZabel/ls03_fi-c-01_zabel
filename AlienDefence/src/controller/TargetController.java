package controller;

import java.util.ListIterator;

import model.Target;
import model.Level;
import model.persistanceDB.TargetDB;

public class TargetController {

	private TargetDB targetDB;

	public TargetController(TargetDB dbLevel) {
		this.targetDB = dbLevel;
	}

	/**
	 * f�gt dem gew�hlten Level ein neues Ziel hinzu
	 * 
	 * @param target
	 *            neues Ziel
	 * @param lvl
	 *            ausgew�hltes Level
	 * @return -1 wenn die Operation nicht geklappt hat, sonst die Target_id des
	 *         neuen Ziels
	 */
	public Target createTarget(Level lvl) {
		// TODO zum Bild des Targets die ID aus der Datenbank holen
		Target target = new Target(10, 10, 50, 50, 0, 90, "ballon.png");
		int image_id = 1;
		int target_id = targetDB.createTarget(target, lvl.getLevel_id(), image_id);
		target.setTarget_id(target_id);
		lvl.getTargets().add(target);

		return target;
	}

	public Target readTarget(int level_id, int target_id) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean updateTarget(Level lvl, Target target) {
		boolean success = false;
		// Target im Level austauschen
		ListIterator<Target> iterator = lvl.getTargets().listIterator();
		while (iterator.hasNext()) {
			if (iterator.next().getTarget_id() == target.getTarget_id()) {
				iterator.set(target);
				success = true;
			}
		}
		// Target in der Persistenz �ndern
		if (success) {
			this.targetDB.updateTarget(target);
		}
		return success;
	}

	public boolean deleteTarget(Level lvl, int target_id) {
		boolean success = false;

		ListIterator<Target> iterator = lvl.getTargets().listIterator();
		while (iterator.hasNext()) {
			if (iterator.next().getTarget_id() == target_id) {
				iterator.remove();
				success = true;
			}
		}

		// Target in Persistenz l�schen
		if (success) {
			this.targetDB.deleteTarget(target_id);
		}

		return success;
	}

}
