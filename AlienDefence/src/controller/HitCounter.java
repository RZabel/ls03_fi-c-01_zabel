package controller;

public class HitCounter {

	private int shots, hit, targets;

	public HitCounter(int target) {
		this.reset(target);
	}

	public int getShots() {
		return shots;
	}

	public int getHit() {
		return hit;
	}

	public int getTargets() {
		return targets;
	}

	public void setTargets(int targets) {
		this.targets = targets;
	}

	public void hit() {
		this.hit++;
		this.shots++;
	}

	public void miss() {
		this.shots++;
	}

	public double getAccuracy() {
		return Math.round((100.0 / this.shots * this.hit) * 100.0) / 100.0;
	}

	public void reset(int target) {
		this.targets = target;
		this.shots = 0;
		this.hit = 0;
	}
}
