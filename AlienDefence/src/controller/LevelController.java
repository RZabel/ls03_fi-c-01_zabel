package controller;

import java.util.List;

import model.Level;
import model.persistanceDB.LevelDB;

public class LevelController {

	private LevelDB levelDB;
	public final String DEFAULT_LEVELNAME = "unnamed";
	public final String DEFAULT_BACKGROUND_PICTURE_URL = "italy.png";
	public final long DEFAULT_DURATION = 90000L;

	public LevelController(LevelDB dbLevel) {
		this.levelDB = dbLevel;
	}

	/**
	 * legt ein neues Level mit den Standardwerten an
	 * 
	 * @return Levelobjekt
	 */
	public Level createLevel() {
		int level_id = levelDB.createLevel(DEFAULT_LEVELNAME, DEFAULT_BACKGROUND_PICTURE_URL, DEFAULT_DURATION);
		return new Level(level_id);
	}

	/**
	 * gibt das konkrete Level zur�ck
	 * 
	 * @param level_id
	 * @return Levelobjekt oder null bei nicht vorhandener Level_id
	 */
	public Level readLevel(int level_id) {
		List<Level> levels = levelDB.readAllLevel();
		for (int i = 0; i < levels.size(); i++) {
			if (level_id == levels.get(i).getLevel_id()) {
				return levels.get(i);
			}
		}
		return null;
	}

	/**
	 * liest alle Level aus der DB
	 * 
	 * @return Levelliste
	 */
	public List<Level> readAllLevel() {
		return this.levelDB.readAllLevel();
	}

	/**
	 * validiert die �nderungen und speichert das Level ab
	 * 
	 * @param lvl
	 */
	public void updateLevel(Level lvl) {
		// TODO write Code to validate Level-Data
		levelDB.updateLevel(lvl);
	}

	/**
	 * l�scht ein Level aus der Datenhaltung
	 * 
	 * @param level_id
	 */
	public void deleteLevel(int level_id) {
		this.levelDB.deleteLevel(level_id);
	}

}
