package controller;

import java.util.LinkedList;
import java.util.List;

import model.Punkt;
import model.Target;
import model.User;
import model.Level;
import model.persistanceDB.TestsDB;

public class GameController {

	private HitCounter hitCounter;
	private Level currLevel;

	private long timer = 0L;
	private long starttimer = 0, endtimer = 0;
	private List<Target> targets;
	private boolean hasWon;
	private boolean setHighscore = true;
	private User currUser;
	private long sumReactionDiffernce;
	private long relReactionDiffernce;

	public GameController(Level selectedLevel, User user) {

		// this.currLevel = new BasicLevel();
		this.currLevel = selectedLevel;
		this.currUser = user;
		this.hitCounter = new HitCounter(currLevel.getTargets().size());
		this.targets = currLevel.getTargets();
		this.hasWon = false;
	}

	public void doLogik(long delta) {
		timer += delta;

		// time for next logic step

		// Section to run once a second
		if (timer >= 1000) {
			timer -= 100;

			if (this.isAllTargetDestroyed()) {
				this.hasWon = true;

			}

			// Datenbankeintrag
			if ((this.timeleft() <= 0 || this.isHasWon()) && this.setHighscore) {

				if (this.getHits() > 0)
					relReactionDiffernce = sumReactionDiffernce / this.getHits();
				else
					relReactionDiffernce = 0;

				// Eventuell Datum hinzuf�gen

				setHighscore = false;

				// F_participant_id, F_level_id, shots, hits, reaction_time
				new TestsDB().createTest(currUser.getP_participant_id(), currLevel.getLevel_id(), this.getShotsFired(),
						this.getHits(), relReactionDiffernce);

			}

		}

		// steps that use only delta

	}

	private boolean isAllTargetDestroyed() {
		for (Target t : this.targets) {
			if (!t.isHit())
				return false;
		}
		return true;
	}

	// Schuss
	public void fireShot(int x, int y) {
		boolean isHit = false;

		// pr�fe, ob Ziele getroffen wurden
		for (Target t : targets) {
			if (t.getHitbox().enthaelt(new Punkt(x, y)) && !t.isHit()) {

				long differnce = time() - t.getTime();
				sumReactionDiffernce += differnce;
				if (differnce < 0)
					continue;
				isHit = true;
				t.setHit(true);
			}
		}

		// getroffen
		if (isHit) {
			this.hitCounter.hit();
		} else
			this.hitCounter.miss();

	}

	public int getShotsFired() {
		return this.hitCounter.getShots();
	}

	public int getHits() {
		return this.hitCounter.getHit();
	}

	public double getAccuracy() {
		return hitCounter.getAccuracy();
	}

	public List<Target> getTargets() {

		// System.out.println(this.targets);
		return (this.targets != null) ? this.targets : (new LinkedList<Target>());
	}

	public boolean isHasWon() {
		return this.hasWon;
	}

	public void setHasWon(boolean hasWon) {
		this.hasWon = hasWon;
	}

	public void startLevel() {
		this.starttimer = System.currentTimeMillis();
		this.endtimer = this.starttimer + currLevel.getDuration();
	}

	public long timeleft() {
		return (starttimer == 0) ? this.currLevel.getDuration() : (this.endtimer - System.currentTimeMillis()) / 1000;
	}

	public long time() {
		return System.currentTimeMillis() - this.starttimer;
	}

	public Level getCurrLevel() {
		return currLevel;
	}

	public void setCurrLevel(Level currLevel) {
		this.currLevel = currLevel;
	}

}
