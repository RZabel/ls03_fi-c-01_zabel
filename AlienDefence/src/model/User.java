package model;

public class User {

	private int p_participant_id;
	private String first_name;
	private String sur_name;
	private int age;
	private String marital_status;
	private double final_grade;
	private int salary_expectations;
	private String city;
	private int postal_code;
	private String login;
	private String password;

	public User(int p_participant_id, String first_name, String sur_name, int age, String marital_status,
			double final_grade, int salary_expectations, String city, int postal_code, String login, String password) {
		super();
		this.p_participant_id = p_participant_id;
		this.first_name = first_name;
		this.sur_name = sur_name;
		this.age = age;
		this.marital_status = marital_status;
		this.final_grade = final_grade;
		this.salary_expectations = salary_expectations;
		this.city = city;
		this.postal_code = postal_code;
		this.login = login;
		this.password = password;
	}

	public User(int p_participant_id, String login, String password) {
		super();
		this.p_participant_id = p_participant_id;
		this.login = login;
		this.password = password;
	}

	public int getP_participant_id() {
		return p_participant_id;
	}

	public void setP_participant_id(int p_participant_id) {
		this.p_participant_id = p_participant_id;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getSur_name() {
		return sur_name;
	}

	public void setSur_name(String sur_name) {
		this.sur_name = sur_name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getMarital_status() {
		return marital_status;
	}

	public void setMarital_status(String marital_status) {
		this.marital_status = marital_status;
	}

	public double getFinal_grade() {
		return final_grade;
	}

	public void setFinal_grade(double final_grade) {
		this.final_grade = final_grade;
	}

	public int getSalary_expectations() {
		return salary_expectations;
	}

	public void setSalary_expectations(int salary_expectations) {
		this.salary_expectations = salary_expectations;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getPostal_code() {
		return postal_code;
	}

	public void setPostal_code(int postal_code) {
		this.postal_code = postal_code;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
