package model;

import java.util.Vector;

public class Test {

	// Attribute
	private int P_test_id;
	private String first_name;
	private String sur_name;
	private int nummerierung;
	private double hitsShots;
	private double hitsTargets;
	private double reactionDuration;
	private double highscoreFormula;

	// Konstruktor
	public Test(int nummerierung, int p_test_id, double hitsShots, double hitsTargets, double reactionDuration,
			double highscoreFormula, String first_name, String sur_name) {
		super();
		this.nummerierung = nummerierung;
		this.P_test_id = p_test_id;
		this.reactionDuration = reactionDuration;
		this.highscoreFormula = highscoreFormula;
		this.first_name = first_name;
		this.sur_name = sur_name;
		this.hitsShots = hitsShots;
		this.hitsTargets = hitsTargets;
	}

	// R�ckgabewert ist Vector mit Zeilendaten.
	public Vector<String> getRowVector() {
		Vector<String> v = new Vector<String>(5);
		v.addElement(String.valueOf(this.nummerierung));
		v.addElement(this.first_name + " " + this.sur_name);
		v.addElement(String.valueOf((int) this.hitsTargets));
		v.addElement(String.valueOf((int) this.hitsShots));
		v.addElement(String.valueOf((int) this.reactionDuration));
		v.addElement(String.valueOf((int) this.highscoreFormula));
		return v;
	}

}
