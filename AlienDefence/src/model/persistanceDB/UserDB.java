package model.persistanceDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDB {

	// Attribute
	private Connection con;
	private ResultSet results;

	private int P_participant_id;
	private String first_name;
	private String sur_name;
	private int age;
	private String marital_status;
	private double final_grade;
	private int salary_expectations;
	private String city;
	private int postal_code;
	private boolean login = false;

	// Konstruktor. Aus der Target Tabelle wird der Fremschl�ssel f�r das Bild
	// �bernommen und die Adresse des Bildes in image gespeichert.
	public UserDB(String name, String pass) {
		DbAccess dba = new DbAccess();
		String url = dba.getUrl() + dba.getDbName();
		try {
			this.con = DriverManager.getConnection(url, dba.getUser(), dba.getPassword());

			this.results = new DbQuery(
					"SELECT * FROM participants WHERE login_name =\"" + name + "\" and password = \"" + pass + "\"",
					con).getResults();
			getUserDb();

		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void getUserDb() {

		try {
			while (this.results.next()) {

				P_participant_id = this.results.getInt("P_participant_id");
				first_name = this.results.getString("first_name");
				sur_name = this.results.getString("sur_name");
				age = this.results.getInt("age");
				postal_code = this.results.getInt("postal_code");
				city = this.results.getString("city");
				salary_expectations = this.results.getInt("salary_expectations");
				marital_status = this.results.getString("marital_status");
				final_grade = this.results.getDouble("final_grade");

				System.out.println(this.results.getInt("P_participant_id"));

				login = true;

			}
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		}
	}

	public int getP_participant_id() {
		return P_participant_id;
	}

	public String getFirst_name() {
		return first_name;
	}

	public String getSur_name() {
		return sur_name;
	}

	public int getAge() {
		return age;
	}

	public String getMarital_status() {
		return marital_status;
	}

	public double getFinal_grade() {
		return final_grade;
	}

	public int getSalary_expectations() {
		return salary_expectations;
	}

	public String getCity() {
		return city;
	}

	public int getPostal_code() {
		return postal_code;
	}

	public boolean isLogin() {
		return login;
	}

}
