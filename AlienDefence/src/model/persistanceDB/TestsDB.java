package model.persistanceDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import model.Test;

public class TestsDB {

	// Attribute
	private Connection con;
	private ResultSet results;
	private Vector<Vector<String>> vecTests;
	private int nummerierung;
	private DbAccess dba = new DbAccess();

	// Konstuktor
	public TestsDB() {
	}

	public TestsDB(int level_id) {
		String url = dba.getUrl() + dba.getDbName();

		String highscoreFormula = "(hits / targets * 1000)*0.4 + (hits / shots * 1000)*0.2 + (1000 - (reaction_time / sum_duration * 1000))*0.4";

		String query = "SELECT P_test_id, targets, shots, hits, reaction_time, first_name, sur_name, (hits / targets * 1000) AS hitsTargets, (hits / shots * 1000) AS hitsShots, (1000 - (reaction_time / sum_duration * 1000)) AS reactionDuration,"
				+ highscoreFormula + "AS highscoreFormula FROM "
				+ "(SELECT F_level_id, COUNT(P_target_id) AS targets, SUM(duration ) AS sum_duration  FROM `targets` WHERE F_level_id = "
				+ level_id + ") AS count_targets INNER JOIN "
				+ "tests ON tests.F_level_id = count_targets.F_level_id INNER JOIN "
				+ "participants ON P_participant_id = F_participant_id ORDER BY highscoreFormula DESC, reactionDuration DESC";

		// System.out.println(query);
		try {
			this.con = DriverManager.getConnection(url, dba.getUser(), dba.getPassword());
			this.results = new DbQuery(query, con).getResults();
			this.vecTests = getVecTest();

		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	// Daten aus der Datenbank werden in einem zweidimensionalen Vector gespeichert.
	public Vector<Vector<String>> getVecTest() {

		Vector<Vector<String>> vecTests = new Vector<Vector<String>>();
		try {
			while (this.results.next()) {

				nummerierung++;

				Test test = new Test(nummerierung, this.results.getInt("P_test_id"),
						this.results.getDouble("hitsShots"), this.results.getDouble("hitsTargets"),
						this.results.getInt("reactionDuration"), this.results.getInt("highscoreFormula"),
						this.results.getString("first_name"), this.results.getString("sur_name"));

				Vector<String> v = test.getRowVector();
				vecTests.add(v);
			}
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		}
		return vecTests;
	}

	public Vector<Vector<String>> getVecTests() {
		return vecTests;
	}

	public void createTest(int F_participant_id, int F_level_id, int shots, int hits, long reaction_time) {

		String url = dba.getFullURL();
		try {

			this.con = DriverManager.getConnection(url, dba.getUser(), dba.getPassword());

			// targets
			String query = "INSERT INTO tests (F_participant_id, F_level_id, shots, hits, reaction_time)"
					+ " values (?, ?, ?, ?, ?)";
			PreparedStatement preparedStmt = con.prepareStatement(query);
			preparedStmt.setInt(1, F_participant_id);
			preparedStmt.setInt(2, F_level_id);
			preparedStmt.setInt(3, shots);
			preparedStmt.setInt(4, hits);
			preparedStmt.setLong(5, reaction_time);
			preparedStmt.execute();

		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}
}