package model.persistanceDB;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DbQuery {

	// Attribute
	private Connection con;
	private ResultSet results;
	String query;

	// Konstruktor, übernimmt Verbindung und Anfrage
	public DbQuery(String query, Connection con) {
		this.con = con;
		this.query = query;
		dbRead();
	}

	// Datensätze werden aus der DB geholt und im Objekt results gespeichert
	public void dbRead() {
		try {
			Statement stmt = con.createStatement();
			this.results = stmt.executeQuery(this.query);
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		}
	}

	public ResultSet getResults() {
		return results;
	}

}
