package model.persistanceDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ImageDB {

	// Attribute
	private Connection con;
	private ResultSet results;
	String image;

	// Konstruktor. Aus der Target Tabelle wird der Fremschl�ssel f�r das Bild
	// �bernommen und die Adresse des Bildes in image gespeichert.
	public ImageDB(int image_id) {
		DbAccess dba = new DbAccess();
		String url = dba.getUrl() + dba.getDbName();
		try {
			this.con = DriverManager.getConnection(url, dba.getUser(), dba.getPassword());

			this.results = new DbQuery("SELECT * FROM images WHERE P_image_id =" + image_id, con).getResults();
			this.image = getImageDb();
			// System.out.println("verb.");

		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	// Bild des Targets wird aus der Tabelle images geholt.
	public String getImageDb() {
		try {
			this.results.next();
			image = this.results.getString("image");
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		}
		return image;
	}

	public String getImage() {
		return image;
	}

}
