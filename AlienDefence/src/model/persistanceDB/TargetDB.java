package model.persistanceDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Vector;

import model.Rechteck;
import model.Target;

public class TargetDB {
	private DbAccess dbAccess;

	public TargetDB(DbAccess dbAccess) {
		super();
		if (dbAccess != null)
			this.dbAccess = dbAccess;
		else
			this.dbAccess = new DbAccess();
	}

	public int createTarget(Target target, int level_id, int image_id) {
		String sql = "INSERT INTO targets (F_level_id, F_image_id, x_position, y_position, width, height, time, duration, color) VALUES (?, ?, ?, ?, ?, ?, ?, ?, '');";
		int lastKey = -1;
		try (Connection con = DriverManager.getConnection(this.dbAccess.getFullURL(), this.dbAccess.getUser(),
				this.dbAccess.getPassword());
				PreparedStatement statement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {

			statement.setInt(1, level_id);
			statement.setInt(2, image_id);
			statement.setInt(3, target.getHitbox().getX());
			statement.setInt(4, target.getHitbox().getY());
			statement.setInt(5, target.getHitbox().getBreite());
			statement.setInt(6, target.getHitbox().getHoehe());
			statement.setLong(7, target.getTime());
			statement.setLong(8, target.getDuration());

			statement.execute();

			ResultSet generatedKeys = statement.getGeneratedKeys();
			if (generatedKeys.next()) {
				lastKey = generatedKeys.getInt(1);
			}
			generatedKeys.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return lastKey;
	}

	public List<Target> getTargets(int level_id) {
		String sql = "SELECT * FROM targets WHERE F_level_id = ? ORDER BY P_target_id;";
		List<Target> alltargets = new Vector<Target>();

		try (Connection con = DriverManager.getConnection(this.dbAccess.getFullURL(), this.dbAccess.getUser(),
				this.dbAccess.getPassword()); PreparedStatement statement = con.prepareStatement(sql)) {

			statement.setInt(1, level_id);

			ResultSet rs = statement.executeQuery();

			while (rs.next()) {

				Target target = new Target();
				target.setTarget_id(rs.getInt("P_target_id"));
				target.setHitbox(new Rechteck(rs.getInt("x_position"), rs.getInt("y_position"), rs.getInt("width"),
						rs.getInt("height")));
				target.setTime(rs.getLong("time"));
				target.setDuration(rs.getLong("duration"));
				// TODO map right URL to foreign key
				// t.setImageAddress(rs.getString("F_image_id"));
				alltargets.add(target);
			}

		} catch (SQLException e) {
			e.getMessage();
			e.printStackTrace();
		}
		return alltargets;
	}

	public void updateTarget(Target target) {
		String sql = "UPDATE targets SET x_position = ?, y_position = ?, width = ?, height = ?, time = ?, duration = ? WHERE P_target_id = ?;";
		try (Connection con = DriverManager.getConnection(this.dbAccess.getFullURL(), this.dbAccess.getUser(),
				this.dbAccess.getPassword()); PreparedStatement statement = con.prepareStatement(sql)) {

			statement.setInt(1, target.getHitbox().getX());
			statement.setInt(2, target.getHitbox().getY());
			statement.setInt(3, target.getHitbox().getBreite());
			statement.setInt(4, target.getHitbox().getHoehe());
			statement.setLong(5, target.getTime());
			statement.setLong(6, target.getDuration());
			statement.setInt(7, target.getTarget_id());

			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void deleteTarget(int target_id) {
		String sql = "DELETE FROM targets WHERE P_target_id = " + target_id + ";";

		try (Connection con = DriverManager.getConnection(this.dbAccess.getFullURL(), this.dbAccess.getUser(),
				this.dbAccess.getPassword()); Statement statement = con.createStatement()) {

			statement.executeUpdate(sql);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
