package model;

import java.util.Random;

public class Target {

	private int target_id;
	/** the area of a target */
	private Rechteck hitbox;
	/** time a target appears after game begins */
	private long time;
	/** time a target is able to hit */
	private long duration;
	private boolean hit;
	private String imageAddress;

	/** image of target */

	public Target() {
		super();
		this.target_id = 0;
		this.hitbox = new Rechteck(10, 10, 50, 50);
		this.time = 0;
		this.duration = Long.MAX_VALUE;
		this.setHit(false);
		this.setImageAddress("ballon.png");
	}

	public Target(int x, int y, int width, int height, long time, long duration, String image) {
		super();
		this.target_id = 0;
		this.hitbox = new Rechteck(x, y, width, height);
		this.time = time;
		this.duration = duration;
		this.setHit(false);
		this.setImageAddress(image);
	}

	public Target(Rechteck hitbox, long time, long duration) {
		super();
		this.target_id = 0;
		this.hitbox = hitbox;
		this.time = time;
		this.duration = duration;
		this.setHit(false);
	}

	public int getTarget_id() {
		return target_id;
	}

	public void setTarget_id(int target_id) {
		this.target_id = target_id;
	}

	public Rechteck getHitbox() {
		return hitbox;
	}

	public void setHitbox(Rechteck hitbox) {
		this.hitbox = hitbox;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public boolean isHit() {
		return hit;
	}

	public void setHit(boolean hit) {
		this.hit = hit;
	}

	public String getImageAddress() {
		return imageAddress;
	}

	public void setImageAddress(String imageAdress) {
		this.imageAddress = imageAdress;
	}

	public static Target getRandomTarget(int screenResolution_X, int screenResolution_Y) {
		Random rand = new Random();
		return new Target(rand.nextInt(screenResolution_X - 50), rand.nextInt(screenResolution_Y - 50), 50, 50, 0,
				90000, "ballon.png");
	}

	public String[] getData() {
		return new String[] { this.target_id + "", this.hitbox.getX() + "", this.hitbox.getY() + "",
				this.hitbox.getBreite() + "", this.hitbox.getHoehe() + "", this.getImageAddress(), this.getTime() + "",
				this.getDuration() + "" };
	}

	public static String[] getTableDescriptions() {
		return new String[] { "Nr.", "X", "Y", "Breite", "H�he", "Bild", "Startzeit", "Dauer" };
	}

	@Override
	public String toString() {
		return "Target [target_id=" + target_id + ", hitbox=" + hitbox + ", time=" + time + ", duration=" + duration
				+ ", hit=" + hit + ", imageAddress=" + imageAddress + "]";
	}

}
