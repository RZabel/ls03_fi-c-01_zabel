-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 20. Mai 2020 um 22:05
-- Server-Version: 10.4.11-MariaDB
-- PHP-Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `assessment`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `images`
--

CREATE TABLE `images` (
  `P_image_id` int(11) NOT NULL,
  `image` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `images`
--

INSERT INTO `images` (`P_image_id`, `image`) VALUES
(1, 'ballon.png'),
(2, 'lemon.jpg');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `levels`
--

CREATE TABLE `levels` (
  `P_level_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `background` varchar(100) NOT NULL,
  `duration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `levels`
--

INSERT INTO `levels` (`P_level_id`, `name`, `background`, `duration`) VALUES
(1, 'Level 1', 'italy.jpg', 15000),
(2, 'Level 2', 'city.jpg', 20000),
(3, 'Level 3', 'italy.jpg', 25000);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `participants`
--

CREATE TABLE `participants` (
  `P_participant_id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `sur_name` varchar(100) NOT NULL,
  `age` int(11) NOT NULL,
  `street` varchar(100) NOT NULL,
  `house_number` int(11) NOT NULL,
  `postal_code` int(11) NOT NULL,
  `city` varchar(100) NOT NULL,
  `login_name` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `salary_expectations` int(100) NOT NULL,
  `marital_status` varchar(100) NOT NULL,
  `final_grade` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `participants`
--

INSERT INTO `participants` (`P_participant_id`, `first_name`, `sur_name`, `age`, `street`, `house_number`, `postal_code`, `city`, `login_name`, `password`, `salary_expectations`, `marital_status`, `final_grade`) VALUES
(1, 'Test', 'Test', 10, 'Testweg', 1, 10245, 'Berlin', 'test', 'pass', 36000, 'Roboter', 1),
(2, 'MC', 'A', 51, 'Brooklynstreet', 2, 10113, 'NYC', 'mc', 'pass', 100000, 'ledig', 2.4),
(3, 'Susanne', 'König', 33, 'Hasenweg', 4, 10245, 'Berlin', 'susi', 'pass', 20000, 'verheirate', 2.9),
(4, 'Jan', 'Jonas', 17, 'Galihag', 111, 10317, 'Berlin', 'jan', 'pass', 44444, 'ledig', 1),
(5, 'Felix', 'DelSande', 21, 'Lerchenweg', 23, 10245, 'Berlin', 'felix', 'pass', 36000, 'ledig', 3.7);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `targets`
--

CREATE TABLE `targets` (
  `P_target_id` int(11) NOT NULL,
  `F_level_id` int(11) NOT NULL,
  `F_image_id` int(11) NOT NULL,
  `x_position` int(11) NOT NULL,
  `y_position` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `color` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `targets`
--

INSERT INTO `targets` (`P_target_id`, `F_level_id`, `F_image_id`, `x_position`, `y_position`, `width`, `height`, `time`, `duration`, `color`) VALUES
(1, 1, 1, 100, 100, 50, 120, 1000, 5000, ''),
(2, 1, 1, 200, 100, 75, 150, 2000, 5000, ''),
(3, 1, 1, 300, 100, 75, 150, 3000, 5000, ''),
(4, 1, 1, 400, 100, 75, 150, 4000, 5000, ''),
(5, 1, 1, 100, 300, 75, 150, 0, 2000, ''),
(6, 1, 1, 200, 300, 75, 150, 0, 3000, ''),
(7, 1, 1, 300, 300, 75, 150, 0, 4000, ''),
(8, 1, 1, 400, 300, 75, 150, 2000, 5000, ''),
(9, 1, 2, 400, 300, 75, 150, 5000, 10000, ''),
(10, 2, 1, 400, 222, 75, 150, 0, 5000, ''),
(11, 2, 1, 333, 555, 75, 150, 0, 5000, ''),
(12, 2, 1, 88, 22, 100, 100, 0, 5000, ''),
(13, 2, 1, 4, 1, 30, 50, 0, 5000, ''),
(14, 2, 1, 777, 667, 150, 300, 0, 2000, ''),
(15, 2, 1, 200, 555, 75, 150, 0, 3000, ''),
(16, 2, 1, 555, 200, 122, 333, 0, 4000, ''),
(17, 2, 1, 400, 567, 75, 150, 2000, 5000, ''),
(18, 2, 1, 400, 300, 75, 150, 5000, 10000, ''),
(19, 3, 1, 100, 100, 75, 150, 0, 5000, '');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tests`
--

CREATE TABLE `tests` (
  `P_test_id` int(11) NOT NULL,
  `F_participant_id` int(11) NOT NULL,
  `F_level_id` int(11) NOT NULL,
  `shots` int(11) NOT NULL,
  `hits` int(11) NOT NULL,
  `reaction_time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `tests`
--

INSERT INTO `tests` (`P_test_id`, `F_participant_id`, `F_level_id`, `shots`, `hits`, `reaction_time`) VALUES
(1, 1, 1, 13, 6, 6532),
(5, 1, 1, 11, 8, 639),
(6, 1, 3, 1, 1, 1050),
(7, 1, 1, 9, 9, 1041),
(8, 1, 1, 9, 9, 1000),
(9, 1, 3, 1, 1, 1153),
(10, 1, 3, 1, 1, 813),
(11, 1, 3, 1, 1, 1535),
(12, 1, 3, 4, 1, 1671),
(13, 1, 3, 1, 1, 875),
(14, 1, 3, 1, 1, 1158),
(15, 1, 3, 14, 1, 4248),
(16, 1, 3, 3, 1, 1296),
(17, 1, 3, 4, 1, 1727),
(18, 1, 3, 4, 1, 2217),
(19, 1, 2, 7, 7, 1247),
(20, 1, 3, 1, 1, 746),
(21, 1, 3, 1, 1, 1111),
(22, 1, 1, 9, 9, 731),
(23, 1, 3, 1, 1, 1345),
(24, 1, 3, 1, 1, 669),
(25, 1, 3, 1, 1, 893),
(26, 1, 3, 1, 1, 1125),
(27, 1, 3, 1, 1, 969),
(28, 1, 3, 1, 1, 1444),
(29, 1, 3, 1, 1, 1122);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`P_image_id`);

--
-- Indizes für die Tabelle `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`P_level_id`);

--
-- Indizes für die Tabelle `participants`
--
ALTER TABLE `participants`
  ADD PRIMARY KEY (`P_participant_id`);

--
-- Indizes für die Tabelle `targets`
--
ALTER TABLE `targets`
  ADD PRIMARY KEY (`P_target_id`),
  ADD KEY `FK IMAGES ID` (`F_image_id`),
  ADD KEY `FK LEVELS ID` (`F_level_id`);

--
-- Indizes für die Tabelle `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`P_test_id`),
  ADD KEY `FK PARTICIPANTS ID` (`F_participant_id`),
  ADD KEY `FK LEVELS2 ID` (`F_level_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `images`
--
ALTER TABLE `images`
  MODIFY `P_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT für Tabelle `levels`
--
ALTER TABLE `levels`
  MODIFY `P_level_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT für Tabelle `participants`
--
ALTER TABLE `participants`
  MODIFY `P_participant_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT für Tabelle `targets`
--
ALTER TABLE `targets`
  MODIFY `P_target_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT für Tabelle `tests`
--
ALTER TABLE `tests`
  MODIFY `P_test_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `targets`
--
ALTER TABLE `targets`
  ADD CONSTRAINT `FK IMAGES ID` FOREIGN KEY (`F_image_id`) REFERENCES `images` (`P_image_id`),
  ADD CONSTRAINT `FK LEVELS ID` FOREIGN KEY (`F_level_id`) REFERENCES `levels` (`P_level_id`) ON UPDATE CASCADE ON DELETE CASCADE;

--
-- Constraints der Tabelle `tests`
--
ALTER TABLE `tests`
  ADD CONSTRAINT `FK LEVELS2 ID` FOREIGN KEY (`F_level_id`) REFERENCES `levels` (`P_level_id`) ON UPDATE CASCADE ON DELETE CASCADE,
  ADD CONSTRAINT `FK PARTICIPANTS ID` FOREIGN KEY (`F_participant_id`) REFERENCES `participants` (`P_participant_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
